The vimcolor module highlights code in many different formats. VIM is a common
programmers text editor for (usually) unix based systems. Out of the box VIM
can color the syntax of 200+ languages including PHP, Perl, C, HTML, Fortran,
Haskell, Java, etc.

This module is similar in functionality to the codefilter.module. In fact,
this module's code was originally a duplicate of codefilter's code and then
modified to fit. The difference being that vimcolor.module is much more
powerful, and takes more steps to install. Some web hosts may not even be able
to support vimcolor.module. If all you need is PHP code coloring, use
codefilter.module.

If you are currently useing codefilter.module you should be able to seamlessly
upgrade to vimcolor.module without having to modify any of your current
content since vimcolor.module supports all of codefilter.module's formatting
commands.

This module requires Perl version 5.6 or better (5.8 is optimal) and the
Text::VimColor module available on CPAN.  The text editor, VIM, is also
required.  From what I can tell from the CPAN testers Text::VimColor does not
run well under Windows

See http://www.bluefeet.net/vimcolor_module to see vimcolor.module in action.

See INSTALL.txt for installation steps.

Send comments to aran@bluefeet.net.


